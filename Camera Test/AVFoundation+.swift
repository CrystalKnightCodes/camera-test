//
//  AVFoundation+.swift
//  BLUE
//
//  Created by June Bash on 2020-11-25.
//  Copyright © 2020 Rekor. All rights reserved.

import AVFoundation
import UIKit
import Combine


extension Data
{
  /// Converts CMSampleBuffer to jpeg image data.
  ///
  /// May need to use autoreleasepool to prevent memory leaks.
  public static func image(from sampleBuffer: CMSampleBuffer, in context: CIContext) -> Data?
  {
    guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
    let ciImage = CIImage(cvPixelBuffer: imageBuffer)
    guard let colorSpace = ciImage.colorSpace else { return nil }
    return context.jpegRepresentation(of: ciImage, colorSpace: colorSpace)
  }
}


extension AVAudioPlayer
{
  /// Initializes a new audio player, looking in the main bundle for a file with the provided name and type.
  public convenience init(
    forFilename filename: String,
    type: String = "mp3",
    in bundle: Bundle = .main
  ) throws {
    guard let path = bundle.path(forResource: filename, ofType: type)
    else { throw SimpleError("Could not get path for audio file") }

    try self.init(contentsOf: URL(fileURLWithPath: path))
  }
}


extension AVCaptureDevice
{
  /// Ask the user for permission to access the device camera, if necessary.
  ///
  /// Will return a publisher which outputs `Void` if the user gave permission to use the camera or if permission was already previously granted. Otherwise will fail with an error.
  static func requestPermission() -> AnyPublisher<Void, CameraError> {
    Future { fulfill in
      switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
      {
        case .authorized:
          fulfill(.success)

        case .notDetermined:
          AVCaptureDevice.requestAccess(for: .video) { didGrant in
            DispatchQueue.main.async {
              return fulfill(didGrant ? .success : .failure(.notAuthorized))
            }
          }

        default:
          fulfill(.failure(CameraError.notAuthorized))
      }
    }
    .eraseToAnyPublisher()
  }
}


extension AVCaptureConnection
{
  /// Sets the connection to the provided video orientation if supported; throws an error otherwise.
  func setVideoOrientation(_ orientation: AVCaptureVideoOrientation) throws
  {
    guard isVideoOrientationSupported
    else { throw CameraError.configurationFailed("Video orientation not supported on connection: \(self)") }
    self.videoOrientation = orientation
  }
}


extension AVCaptureSession
{
  /// Initializes and configures a new AVCaptureSession using the provided items.
  static func configuredForVideo(
    quality: AVCaptureSession.Preset = .hd1280x720,
    position: AVCaptureDevice.Position = .back,
    delegateData: (AVCaptureVideoDataOutputSampleBufferDelegate, DispatchQueue)? = nil
  ) throws -> AVCaptureSession {
    try AVCaptureSession() |> {
      try $0.configureForVideo(quality: quality, position: position, delegateData: delegateData)
    }
  }

  /// Initializes and configures a new AVCaptureSession using the provided closure, which will be wrapped in `beginConfiguration` and `commitConfiguration`
  convenience init(_ configure: (AVCaptureSession) throws -> Void) rethrows
  {
    self.init()
    try configure(self)
  }

  /// Add an input if possible, otherwise throw an error.
  func safeAddInput(_ input: AVCaptureDeviceInput) throws
  {
    guard canAddInput(input)
    else { throw CameraError.configurationFailed("Couldn't add video device input to the session.") }

    addInput(input)
  }

  /// Add the default video input at the provided position if possible; otherwise, throw an error.
  func addNewVideoInput(position: AVCaptureDevice.Position) throws
  {
    try AVCaptureDevice.DiscoverySession(
      deviceTypes: [.builtInTelephotoCamera, .builtInWideAngleCamera],
      mediaType: .video,
      position: position
    )
    .devices.first
    .map(AVCaptureDeviceInput.init(device:) >>> safeAddInput(_:))
    ?! CameraError.configurationFailed("Could not create capture device input")
  }

  /// Add an output if possible, otherwise throw an error.
  func safeAddOutput(_ output: AVCaptureOutput) throws
  {
    guard canAddOutput(output)
    else { throw CameraError.configurationFailed("Could not add photo output to the session") }

    addOutput(output)
  }

  /// Add the default video input at the provided position if possible; otherwise, throw an error.
  func addNewVideoOutput(
    orientation: AVCaptureVideoOrientation = .portrait,
    delegateData: (AVCaptureVideoDataOutputSampleBufferDelegate, DispatchQueue)? = nil
  ) throws {
    let output = AVCaptureVideoDataOutput()
    if let data = delegateData
    {
      output.setSampleBufferDelegate(data.0, queue: data.1)
    }
    try safeAddOutput(output)
    let connection = try output.connection(with: .video)
      ?! CameraError.configurationFailed("Failed to get connection from new video output")
    try connection.setVideoOrientation(orientation)
  }

  /// Wraps the provided closure in `beginConfiguration` and `commitConfiguration`, so that there's no need to remember to call `commitConfiguration`.
  func configure<T>(_ configuration: () throws -> T) rethrows -> T
  {
    beginConfiguration()
    defer { commitConfiguration() }
    let output = try configuration()
    return output
  }

  /// Configures the session using the provided items.
  func configureForVideo(
    quality: AVCaptureSession.Preset = .hd1280x720,
    position: AVCaptureDevice.Position = .back,
    delegateData: (AVCaptureVideoDataOutputSampleBufferDelegate, DispatchQueue)? = nil
  ) throws {
    guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized
    else { throw CameraError.notAuthorized }

    try configure {
      sessionPreset = quality
      try addNewVideoInput(position: position)
      try addNewVideoOutput(orientation: .portrait, delegateData: delegateData)
    }
  }
}

public struct SimpleError: LocalizedError
{
  public let errorDescription: String?
  public let failureReason: String?
  public let helpAnchor: String?
  public let recoverySuggestion: String?

  public init(
    description: String? = nil,
    failureReason: String? = nil,
    helpAnchor: String? = nil,
    recoverySuggestion: String? = nil
  ) {
    self.errorDescription = description
    self.failureReason = failureReason
    self.helpAnchor = helpAnchor
    self.recoverySuggestion = recoverySuggestion
  }

  public init(_ errorDescription: String) {
    self.init(description: errorDescription)
  }
}
