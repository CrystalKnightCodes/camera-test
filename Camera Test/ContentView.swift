//
//  ContentView.swift
//  Camera Test
//
//  Created by Crystal Knight on 6/29/21.
//

import SwiftUI
import UIKit
import AVFoundation
import Combine

struct ContentView: View
{
  @State var session: AVCaptureSession
  @State var hasPermission: Bool = false
  @State var camera: OnDeviceCameraManager?
  @State var cameraError: CameraError?
  @State var continuousAutoFocusOn: Bool = false
  @State var telephotoOn: Bool = false
  @State var minimumFocusDistance: Double = 0
  @State var autoFocusRangeRestriction: AVCaptureDevice.AutoFocusRangeRestriction = .near
  @State var continuousAutoExposureOn: Bool = false
  @State var exposureTargetBias: Float = 0
  
  init() {
    session = AVCaptureSession()
  }
  
  
  var body: some View {
    ZStack(alignment: .center) {
      CameraWrapper(session: session)
        .ignoresSafeArea()
        .aspectRatio(contentMode: .fill)
        .onAppear { didAppear() }
        .onDisappear { didDisappear() }
      
      VStack(alignment: .center, spacing: 20) {
        Text("Camera Settings")
          .font(.system(.title))
          .padding()
        
        VStack {
          Text("Focus")
            .bold()
          
          Divider()
          
          Toggle("Continuous Auto Focus On", isOn: $continuousAutoFocusOn)
          
          HStack {
            Text("Focus Distance")
            
            Slider(value: $minimumFocusDistance, in: 0...100, step: 1)
              .accentColor(.blue)
              .padding(5)
              .frame(maxWidth: 200)
              .overlay(RoundedRectangle(cornerRadius: 16)
                        .stroke(lineWidth: 2)
                        .foregroundColor(.blue)
              )
          } //: Focus Distance HStack
          
          Picker("Auto Focus Range", selection: $autoFocusRangeRestriction) {
            let text = ["None", "Near", "Far"]
            ForEach(AVCaptureDevice.AutoFocusRangeRestriction.allCases, id: \.rawValue) { value in
              Text(text[value.rawValue]).tag(value)
            }
          }
        }
        
        VStack {
          Text("Exposure")
            .bold()
          
          Divider()
          
          Toggle("Continuous Auto Exposure On", isOn: $continuousAutoExposureOn)
          
          HStack {
            Text("Exposure Target Bias")
            
            Slider(value: $exposureTargetBias, in: 0...100, step: 1)
              .accentColor(.purple)
              .padding(5)
              .frame(maxWidth: 200)
              .overlay(RoundedRectangle(cornerRadius: 16)
                        .stroke(lineWidth: 2)
                        .foregroundColor(.purple)
              )
          } //: Shutter Speed HStack
        }
        Text(cameraError?.localizedDescription ?? "No errors")
        Spacer()
      } //: Main VStack
      .frame(maxWidth: UIScreen.main.bounds.width)
      .padding()
      .background(Color.clear)
      .pickerStyle(SegmentedPickerStyle())
      .toggleStyle(SwitchToggleStyle())
    } //: ZStack
  }
  
  private struct CameraWrapper: UIViewRepresentable
  {
    var session: AVCaptureSession
    
    func makeUIView(context: Context) -> CameraView
    {
      CameraView(session: session)
    }
    
    func updateUIView(_ uiView: ContentView.CameraView, context: Context) {
      uiView.videoPreviewLayer.session = session
    }
  }
  
  class CameraView: UIView
  {
    override class var layerClass: AnyClass { AVCaptureVideoPreviewLayer.self }
    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
      layer as! AVCaptureVideoPreviewLayer
    }
    var session: AVCaptureSession? { videoPreviewLayer.session }
    
    init(frame: CGRect = .zero, session: AVCaptureSession) {
      super.init(frame: frame)
      videoPreviewLayer.session = session
      session.startRunning()
    }
    
    required init?(coder: NSCoder) {
      super.init(coder: coder)
    }
  }
  
  func didAppear()
  {
      requestPermission()
      configureSession()
  }
  
  func requestPermission()
  {
    switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    {
      case .authorized:
        hasPermission = true
      case .notDetermined:
        AVCaptureDevice.requestAccess(for: .video) { granted in
          self.hasPermission = granted
        }
      default:
        hasPermission = false
        cameraError = .notAuthorized
    }
  }
  
  func configureSession()
  {
    guard hasPermission else { cameraError = .notAuthorized; return }
    session.beginConfiguration()
    session.sessionPreset = .hd1280x720
    
    guard let defaultDevice = AVCaptureDevice.default(
      telephotoOn ? .builtInTelephotoCamera : .builtInWideAngleCamera,
      for: .video, position: .back
    ) else {
      cameraError = .configurationFailed("Failed to find default device")
      return
    }
    
    do {
      try defaultDevice.lockForConfiguration()
    } catch {
      cameraError = .configurationFailed("Unable to lock for configuration.")
    }
    
    if defaultDevice.isAutoFocusRangeRestrictionSupported {
      defaultDevice.autoFocusRangeRestriction = autoFocusRangeRestriction }
    if defaultDevice.isExposureModeSupported(.continuousAutoExposure) {
      defaultDevice.exposureMode = continuousAutoFocusOn ? .continuousAutoExposure : .autoExpose }
    if defaultDevice.isLowLightBoostSupported {
      defaultDevice.automaticallyEnablesLowLightBoostWhenAvailable = true }
    defaultDevice.setExposureTargetBias(exposureTargetBias, completionHandler: { time in
      // ???
    })
    defaultDevice.unlockForConfiguration()
    
    guard let deviceInput = try? AVCaptureDeviceInput(device: defaultDevice) else {
      cameraError = .configurationFailed("Device input does not exist")
      return
    }
    
    if session.canAddInput(deviceInput) {
      session.addInput(deviceInput)
    } else
    {
      cameraError = .configurationFailed("Cannot add device input")
    }
    session.commitConfiguration()
    session.startRunning()
  }
  
  func didFail(error: CameraError)
  {
    
  }
  
  func didDisappear()
  {
    session.stopRunning()
  }

}

extension AVCaptureDevice.AutoFocusRangeRestriction: CaseIterable {
  public static var allCases: [AVCaptureDevice.AutoFocusRangeRestriction] = [.none, .near, .far]
}


