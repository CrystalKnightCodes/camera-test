//  CameraManager.swift
//  BOLO
//
//  Created by June Bash on 2021-01-05.
//

import AVFoundation
import Combine


/// A type of lens to be used with a `CameraManager`.
public enum CameraLensType: Int, CaseIterable
{
  case wideAngle = 0, telephoto = 1

  var deviceType: AVCaptureDevice.DeviceType {
    switch self {
      case .wideAngle: return .builtInWideAngleCamera
      case .telephoto: return .builtInTelephotoCamera
    }
  }

  func defaultDevice(
    mediaType: AVMediaType = .video,
    position: AVCaptureDevice.Position = .back
  ) -> AVCaptureDevice? {
    AVCaptureDevice.default(deviceType, for: mediaType, position: position)
  }
}


/// A structure containing references and actions relating to camera setup and data.
///
/// The generic `Session` will generally be an `AVCaptureSession` or something similar which wraps lower level access to a camera stream.
public struct CameraManager<Session>
{
  /// A publisher that outputs individual frames of video data.
  ///
  /// The published data may be in any format, but will generally be JPEG or something similar.
  public let framePublisher: AnyPublisher<Data, CameraError>
  /// Ask the user for permission to access the device camera, if necessary.
  ///
  /// Will return a publisher which outputs `Void` if the user gave permission to use the camera or if permission was already previously granted. Otherwise will fail with an error.
  public let requestPermission: () -> AnyPublisher<Void, CameraError>
  /// Configures a `Session` and returns it if configuration was successful, otherwise outputs an error.
  ///
  /// Will generally require that permission is already requested.
  public let configuredSession: () -> Result<Session, CameraError>
  /// Takes in a `CameraLensType` and attempts to switch the camera lens currently being used. If the camera lens cannot be changed, the `Result` will contain an error.
  ///
  /// Will generally require that permission is already requested and the session is already configured.
  public let changeLens: (CameraLensType) -> Result<Void, CameraError>
}

// MARK: - Implementation

public typealias OnDeviceCameraManager = CameraManager<AVCaptureSession>


public extension OnDeviceCameraManager
{
  /// A `CameraManager` which works with the device's internal camera(s).
  static func live(quality: AVCaptureSession.Preset = .hd1280x720) -> CameraManager
  {
    let framePublisher = FramePublisher()

    // By declaring this here and using it within the closures passed into the initializer, it's 'captured' and retained by the closures, essentially acting the same as private instance properties
    var session: AVCaptureSession?

    return CameraManager(
      framePublisher: framePublisher.eraseToAnyPublisher(),
      requestPermission: AVCaptureDevice.requestPermission,
      configuredSession: {
        Result {
          // will use the same session every time this is called, since it's been captured in the closure;
          // if we've already successfully configured it, it will simply be returned here
          // otherwise, it will attempt to configure a new session
          try session ??= .configuredForVideo(
            quality: quality,
            position: .back,
            delegateData: (framePublisher, DispatchQueue(label: "CameraSampleBuffer"))
          )
        }
        .mapError(CameraError.init)
      },

      changeLens: { newLens in
        return Result {
          // get the captured session; if nil, throw an error
          let captureSession = try session ?! CameraError.sessionNotConfigured
          // throw an error if not authorized
          try (AVCaptureDevice.authorizationStatus(for: .video) == .authorized) ?! CameraError.notAuthorized

          return try captureSession.configure {
            // remove old input
            try captureSession.inputs.first
              .map(captureSession.removeInput(_:))
              ?! CameraError.configurationFailed
            // add input for new lens
            try newLens.defaultDevice()
              .map(AVCaptureDeviceInput.init(device:) >>> captureSession.safeAddInput(_:))
              ?! CameraError.optionUnavailable
            // reset all connections to portrait
            captureSession.connections.forEach { $0.videoOrientation = .portrait }
          }
        }.mapError(CameraError.init)
      }
    )
  }
}

public enum CameraError: Error, Equatable
{
  case notAuthorized
  case optionUnavailable
  case sessionNotConfigured
  case configurationFailed(String?)
  case frameConversionFailed
  case nsError(NSError)

  public static var configurationFailed: CameraError { .configurationFailed(nil) }

  public init(_ error: Error)
  {
    self = error as? CameraError ?? .nsError(error as NSError)
  }
}

