//
//  Operators.swift
//  BLUE
//
//  Created by June Bash on 2020-11-17.
//  Copyright © 2020 Rekor. All rights reserved.

import Foundation


precedencegroup PipeForwardPrecedence
{
  associativity: left
  higherThan: AssignmentPrecedence
  lowerThan: NilCoalescingPrecedence
}

precedencegroup ForwardCompositionPrecedence
{
  higherThan: PipeForwardPrecedence
  associativity: left
}

precedencegroup CombinatorialCompositionPrecedence
{
  higherThan: ForwardCompositionPrecedence
  associativity: left
}

infix operator |>: PipeForwardPrecedence // transform; return (immutable)
infix operator =>: PipeForwardPrecedence // mutate in place
infix operator |=>: PipeForwardPrecedence // copy; mutate; return copy

infix operator >>>: ForwardCompositionPrecedence

infix operator <>: CombinatorialCompositionPrecedence

infix operator ?!: NilCoalescingPrecedence

infix operator ??=: NilCoalescingPrecedence


// MARK: - Operations

/// Pipes the left hand item into a closure which takes it in.
///
/// This may look useless at first glance, but it can help avoid obnoxiously nested function calls,
/// especially when used with composition operators (e.g. '`>>>`').
///
/// ```
/// completion(.success(translate(data)))
/// ```
/// vs.
/// ```
/// let translated = translate(data)
/// let result = Result.success(translated)
/// completion(result)
/// ```
/// vs.
/// ```
/// data
///   |> translate
///   >>> Result.success
///   >>> completion
/// ```
///
public func |> <Input, Output>(
  _ input: Input,
  _ transform: (Input) throws -> Output
) rethrows -> Output {
  try transform(input)
}

public func => <Item>(_ item: inout Item, _ f: (inout Item) -> Void)
{
  f(&item)
}

@discardableResult
public func |> <Object: AnyObject>(object: Object, f: (Object) throws -> Void) rethrows -> Object {
  try f(object)
  return object
}



/// Makes a copy of the left-hand item, mutates it with the right-hand closure, and returns the mutated copy.
///
/// Example:
///
/// ```
/// func blue() -> some View {
///   var copy = self
///   copy.textColor = .blue
///   copy.borderColor = .blue
///   return copy
/// }
/// ```
/// vs:
/// ```
/// func blue() -> some View {
///   self |=> {
///     $0.textColor = .blue
///     $0.borderColor = .blue
///   }
/// }
/// ```
///
/// - Parameters:
///   - item: Any item to be copied and mutated.
///   - f: A closure taking in an `inout` item and mutating it.
/// - Returns: The mutated copy of the original item.
public func |=> <Item>(_ item: Item, _ f: (inout Item) -> Void) -> Item
{
  var copy = item
  f(&copy)
  return copy
}

/// Compose two functions together into a new function that takes in the input of the first function and spits out the output of the second function.
///
/// The following statements are all equivalent
/// ```
/// a |> f >>> g
/// (f >>> g)(a)
/// (f(_:) >>> g(_:))(a)
/// g(f(a))
/// ```
///
/// A further example:
///
/// ```
/// func halveAsDouble(_ input: Int) -> Double {
///   Double(input) * 0.5
/// }
///
/// let halvedString = halveAsDouble >>> String.init(_:)
/// 5 |> halvedString   // 2.5
/// // equivalent to:
/// // halvedString(5)
/// // 5 |> halveAsDouble >>> String.init(_:)
/// // String(halveAsDouble(5))
/// ```
///
/// This can be especially useful in chaining several `map` operations. Often, using map several times in a row (e.g., `collection.map(addRelativePath).map(trimURLString).map(NetworkRequest.init)`) could lead to performance issues since it must loop through the collection with each use of `map`. This could be solved by using this operator (e.g., `collection.map(addRelativePath >>> trimURLString >>> NetworkRequest.init)`).
public func >>> <A, B, C>(
  _ f: @escaping (A) -> B,
  _ g: @escaping (B) -> C
) -> (A) -> C {
  { a in g(f(a)) }
}

public func >>> <A, B, C>(
  _ f: @escaping (A) throws -> B,
  _ g: @escaping (B) throws -> C
) -> (A) throws -> C {
  { a in try g(f(a)) }
}

public func >>> <A, B, C>(
  keyPath: KeyPath<A, B>,
  f: @escaping (B) -> C
) -> (A) -> C {
  { a in f(a[keyPath: keyPath]) }
}

public func >>> <A, B, C>(
  f: @escaping (A) -> B,
  keyPath: KeyPath<B, C>
) -> (A) -> C {
  { a in f(a)[keyPath: keyPath] }
}


/// Composes two closures/functions which modify a mutable `inout` parameter.
/// - Parameters:
///   - lhs: A closure which modifies the provided `inout` parameter.
///   - rhs: A closure of the same mutable type.
/// - Returns: A closure which applies both original closures consecutively.
public func <> <Item>(
  _ lhs: @escaping (inout Item) -> Void,
  _ rhs: @escaping (inout Item) -> Void
) -> (inout Item) -> Void {
  { item in
    lhs(&item)
    rhs(&item)
  }
}


/// Attempts to unwrap the provided optional, and if nil, throws the error on the right.
public func ?! <Wrapped>(
  optionalItem: @autoclosure () throws -> Wrapped?,
  error: @autoclosure () -> Error
) throws -> Wrapped {
  guard let item = try optionalItem() else { throw error() }
  return item
}

/// Evaluates the condition on the left, and if false, throws an error.
public func ?!(condition: @autoclosure () throws -> Bool, error: @autoclosure () -> Error) throws
{
  guard try condition() else { throw error() }
}


/// If the optional left-hand value is non-nil, that value will be returned.
/// Otherwise, the right-hand value will both be assigned to the left-hand value _and_ be returned.
///
/// Example:
/// ```
/// var text: String?
/// let message = text ??= "My message" // sets both `text` and `message` to "My message"
/// let otherMessage = text ??= "Other message" // sets `otherMessage` to "My message"; does not change `text`
/// ```
public func ??= <Wrapped>(
  optionalItem: inout Optional<Wrapped>,
  valueIfNil: @autoclosure () throws -> Wrapped
) rethrows -> Wrapped {
  if let item = optionalItem { return item }
  let newValue = try valueIfNil()
  optionalItem = newValue
  return newValue
}
