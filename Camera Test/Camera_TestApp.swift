//
//  Camera_TestApp.swift
//  Camera Test
//
//  Created by Crystal Knight on 6/29/21.
//

import SwiftUI

@main
struct Camera_TestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
