//
//  Result+.swift
//  Rekor Go
//
//  Created by June Bash on 2020-10-30.
//  Copyright © 2020 Rekor. All rights reserved.
//

import Foundation

public extension Result
{
  var isSuccess: Bool {
    if case .success = self { return true } else { return false }
  }

  var error: Failure? {
    if case .failure(let e) = self { return e } else { return nil }
  }

  static func zip<OA, OB, Failure: Error>(
    _ resultA: Result<OA, Failure>,
    _ resultB: Result<OB, Failure>
  ) -> Result<(OA, OB), Failure> {
    resultA.flatMap { oa in
      resultB.flatMap { ob in
        .success((oa, ob))
      }
    }
  }

  static func zip<OA, OB, Failure: Error>(
    _ resultA: Result<OA, Failure>,
    _ outputB: OB
  ) -> Result<(OA, OB), Failure> {
    resultA.flatMap { oa in
      .success((oa, outputB))
    }
  }

  func zip<Other>(with other: Result<Other, Failure>) -> Result<(Success, Other), Failure> {
    Result.zip(self, other)
  }

  func zip<Other>(with other: Other) -> Result<(Success, Other), Failure> {
    Result.zip(self, other)
  }
}

public extension Result where Success == Void
{
  static var success: Result { .success(()) }
}
