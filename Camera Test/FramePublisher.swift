//
//  FramePublisher.swift
//  BOLO
//
//  Created by June Bash on 2020-12-18.
//

import Combine
import AVFoundation
import CoreImage


// MARK: - Frame Publisher

class FramePublisher: NSObject, Publisher, AVCaptureVideoDataOutputSampleBufferDelegate
{
  typealias Output = Data
  typealias Failure = CameraError

  private let context = CIContext()
  private var subscriptions: [CombineIdentifier: FramePublisher.Subscription] = [:]

  deinit {
    subscriptions.values.forEach { sub in
      sub.cancel()
    }
  }

  func receive<S: Subscriber>(subscriber: S) where S.Failure == CameraError, S.Input == Data
  {
    let subscription = Subscription(publisher: self, subscriber: subscriber)
    subscriptions[subscription.combineIdentifier] = subscription
    subscriber.receive(subscription: subscription)
  }

  func captureOutput(
    _ output: AVCaptureOutput,
    didOutput sampleBuffer: CMSampleBuffer,
    from connection: AVCaptureConnection
  ) {
    guard !subscriptions.isEmpty else { return }

    autoreleasepool {
      guard let data = Data.image(from: sampleBuffer, in: context)
      else { return } // complete(with: .failure(.frameConversionFailed)) // fail on bad frame conversion?

      subscriptions.values.forEach { subscription in
        // add the demand of this reception to the total demand, then fulfill
        subscription.provide(data)
      }
    }
  }

  func subscriberDidCancel(with identifier: CombineIdentifier)
  {
    subscriptions.removeValue(forKey: identifier)
  }
}


extension FramePublisher
{
  class Subscription: Combine.Subscription
  {
    private let publisher: FramePublisher
    private var subscriber: AnySubscriber<Data, CameraError>?
    private var currentDemand: Subscribers.Demand = .none

    init<S: Subscriber>(publisher: FramePublisher, subscriber: S)
    where S.Input == Data, S.Failure == CameraError
    {
      self.publisher = publisher
      self.subscriber = AnySubscriber(subscriber)
    }

    func request(_ demand: Subscribers.Demand)
    {
      guard subscriber != nil else { return }
      self.currentDemand += demand
    }

    func cancel()
    {
      publisher.subscriberDidCancel(with: self.combineIdentifier)
      subscriber = nil
    }

    fileprivate func provide(_ data: Data) {
      guard let sub = subscriber else { return }

      currentDemand -= 1
      let newDemand = sub.receive(data)

      guard newDemand > 0 else { return }

      currentDemand += newDemand
    }
  }
}
